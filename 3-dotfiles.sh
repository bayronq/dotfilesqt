#!/bin/bash
#      _       _    __ _ _
#   __| | ___ | |_ / _(_) | ___  ___
#  / _` |/ _ \| __| |_| | |/ _ \/ __|
# | (_| | (_) | |_|  _| | |  __/\__ \
#  \__,_|\___/ \__|_| |_|_|\___||___/
#
# by Stephan Raabe (2023)
# Mod bayronq: cambio de directorio de clonación: ~/Repo/dotfilesqt
# ------------------------------------------------------
# Install Script for Repo/dotfilesqt and configuration
# yay must be installed
# ------------------------------------------------------

# ------------------------------------------------------
# Load Library
# ------------------------------------------------------
source $(dirname "$0")/scripts/library.sh
clear
echo "     _       _    __ _ _            "
echo "  __| | ___ | |_ / _(_) | ___  ___  "
echo " / _' |/ _ \| __| |_| | |/ _ \/ __| "
echo "| (_| | (_) | |_|  _| | |  __/\__ \ "
echo " \__,_|\___/ \__|_| |_|_|\___||___/ "
echo "                                    "
echo "by Stephan Raabe (2023)"
echo "-------------------------------------"
echo ""
echo "The script will ask for permission to remove existing folders and files."
echo "But you can decide to keep your local versions by answering with No (Nn)."
echo "Symbolic links will be created from ~/Repo/dotfilesqt into your home and .config directories."
echo ""

# ------------------------------------------------------
# Confirm Start
# ------------------------------------------------------
while true; do
	read -p "DO YOU WANT TO START THE INSTALLATION NOW? (Yy/Nn): " yn
	case $yn in
	[Yy]*)
		echo "Installation started."
		break
		;;
	[Nn]*)
		exit
		break
		;;
	*) echo "Please answer yes or no." ;;
	esac
done

# ------------------------------------------------------
# Create .config folder
# ------------------------------------------------------
echo ""
echo "-> Check if .config folder exists"

if [ -d ~/.config ]; then
	echo ".config folder already exists."
else
	mkdir ~/.config
	echo ".config folder created."
fi

# ------------------------------------------------------
# Create symbolic links
# ------------------------------------------------------
# name symlink source target

echo ""
echo "-------------------------------------"
echo "-> Install general Repo/dotfilesqt"
echo "-------------------------------------"
echo ""

_installSymLink alacritty ~/.config/alacritty ~/Repo/dotfilesqt/alacritty/ ~/.config
_installSymLink ranger ~/.config/ranger ~/Repo/dotfilesqt/ranger/ ~/.config
_installSymLink vim ~/.config/vim ~/Repo/dotfilesqt/vim/ ~/.config
_installSymLink nvim ~/.config/nvim ~/Repo/dotfilesqt/nvim/ ~/.config
_installSymLink starship ~/.config/starship.toml ~/Repo/dotfilesqt/starship/starship.toml ~/.config/starship.toml
_installSymLink rofi ~/.config/rofi ~/Repo/dotfilesqt/rofi/ ~/.config
_installSymLink dunst ~/.config/dunst ~/Repo/dotfilesqt/dunst/ ~/.config
_installSymLink wal ~/.config/wal ~/Repo/dotfilesqt/wal/ ~/.config
wal -i screenshots/
echo "Pywal templates initiated!"
echo ""
echo "-------------------------------------"
echo "-> Install GTK Repo/dotfilesqt"
echo "-------------------------------------"
echo ""

_installSymLink .gtkrc-2.0 ~/.gtkrc-2.0 ~/Repo/dotfilesqt/gtk/.gtkrc-2.0 ~/.gtkrc-2.0
_installSymLink gtk-3.0 ~/.config/gtk-3.0 ~/Repo/dotfilesqt/gtk/gtk-3.0/ ~/.config/
_installSymLink .Xresouces ~/.Xresources ~/Repo/dotfilesqt/gtk/.Xresources ~/.Xresources
_installSymLink .icons ~/.icons ~/Repo/dotfilesqt/gtk/.icons/ ~/

echo "-------------------------------------"
echo "-> Install Qtile Repo/dotfilesqt"
echo "-------------------------------------"
echo ""

_installSymLink qtile ~/.config/qtile ~/Repo/dotfilesqt/qtile/ ~/.config
_installSymLink polybar ~/.config/polybar ~/Repo/dotfilesqt/polybar/ ~/.config
_installSymLink picom ~/.config/picom ~/Repo/dotfilesqt/picom/ ~/.config
_installSymLink .xinitrc ~/.xinitrc ~/Repo/dotfilesqt/qtile/.xinitrc ~/.xinitrc

echo "-------------------------------------"
echo "-> Install Hyprland Repo/dotfilesqt"
echo "-------------------------------------"
echo ""

_installSymLink hypr ~/.config/hypr ~/Repo/dotfilesqt/hypr/ ~/.config
_installSymLink waybar ~/.config/waybar ~/Repo/dotfilesqt/waybar/ ~/.config
_installSymLink swaylock ~/.config/swaylock ~/Repo/dotfilesqt/swaylock/ ~/.config
_installSymLink wlogout ~/.config/wlogout ~/Repo/dotfilesqt/wlogout/ ~/.config
_installSymLink swappy ~/.config/swappy ~/Repo/dotfilesqt/swappy/ ~/.config

# ------------------------------------------------------
# DONE
# ------------------------------------------------------
echo "DONE! Please reboot your system!"
